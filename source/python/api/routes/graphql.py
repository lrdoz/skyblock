"""
The file defines graphQL route for API
"""

import re

import graphene
from fastapi import APIRouter
from starlette.graphql import GraphQLApp
from graphene_mongo import MongoengineConnectionField

from api.entities.auction import Auction
from api.entities.bin import Bin

class OrderMongoengine(MongoengineConnectionField):
    """
    The class defines order implementation mongoengine connection for graphene interface
    """

    order_field = ""

    @property
    def order_by(self):
        return self.order_field

    @classmethod
    def connection_resolver(cls, resolver, connection_type, root, info, **args):
        cls.order_field = re.sub(r'(?<!^)(?=[A-Z])', '_', args.pop("order_by")).lower()
        return super().connection_resolver(resolver, connection_type, root, info, **args)

class Query(graphene.ObjectType):
    """
    The class defines query for graphQL application
    """
    auction = OrderMongoengine(Auction, order_by = graphene.String(default_value = ""))
    bin = OrderMongoengine(Bin, order_by = graphene.String(default_value = ""))

ROUTER = APIRouter()
ROUTER.add_route("/", GraphQLApp(schema=graphene.Schema(query=Query, types=[])))
