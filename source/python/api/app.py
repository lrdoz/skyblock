"""
The file defines application server
"""

import uvicorn
from fastapi import FastAPI

from starlette.responses import Response
from starlette.middleware.cors import CORSMiddleware
from mongoengine import connect

from api.context import Context
import api.routes.graphql as graphql

# Get context data and init app
CTX = Context()
APP = FastAPI(docs_url=CTX.prefix_url+"/docs", openapi_url=CTX.prefix_url+"/openapi.json",
              redoc_url=CTX.prefix_url+"/redoc", title=CTX.project_name, version=CTX.version)

connect(CTX.database, host=CTX.bdd_host)

#Add conf for allow cross domaine
APP.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

#API product
APP.include_router(
    graphql.ROUTER,
    prefix=CTX.prefix_url+"/graphql",
    tags=["GraphQL"]
)

@APP.get(CTX.prefix_url+'/version')
def get_version(response: Response):
    """
    Retrieve application version

    :param response: Response object of app
    """
    response.status_code = 200
    return {"version": CTX.version}

if __name__ == "__main__":
    uvicorn.run(APP, host=CTX.host, port=CTX.port, log_config=CTX.log_conf,log_level=CTX.log_level)
