"""
The file defines context of application
"""

import logging
import os
import yaml

class Context:
    """
    Class defines variable of app
    """
    def __init__(self):
        #Init log
        self.configure_log()

        #Server configuration
        self.host = os.environ.get("HTTP_HOST", default="0.0.0.0")
        self.port = int(os.environ.get("HTTP_PORT", default="5001"))
        self.prefix_url = os.environ.get("HTTP_PREFIX", default="")

        bdd_url = os.environ.get("MONGO_HOST", default="localhost")
        bdd_port = os.environ.get("MONGO_PORT", default="27017")
        self.bdd_host = "mongodb://{0}:{1}".format(bdd_url, bdd_port)
        self.database = os.environ.get("MONGO_DATABASE", default="skyblock")

        #Project conf
        self.project_name = os.environ.get("PROJECT_NAME", default="GraphQL API")
        self.version = os.environ.get("VERSION", default="1.0")

    def configure_log(self):
        """
        Load log configuration for uvicorn serve
        """
        self.log_level = os.environ.get("LOGGING", default=logging.INFO)
        log_conf_path = os.environ.get("LOGGING_CONF", default="api/log.yaml")

        self.log_conf = yaml.safe_load(open(log_conf_path)) \
            if os.path.exists(log_conf_path) else None
