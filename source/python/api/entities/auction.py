"""
The filed defines auction mongo model
"""

from mongoengine.fields import BooleanField, Document

from graphene_mongo import MongoengineObjectType
from graphene import Node

from api.entities.auction_house import AuctionHouseModel, EnchantmentModel, BidsModel

class Enchantment(MongoengineObjectType):
    """
    The class defines configuration with mongoengine DAO
    """
    class Meta:
        """
        The class defines meta node model
        """
        model = EnchantmentModel
        interfaces = (Node,)

class Bids(MongoengineObjectType):
    """
    The class defines configuration with mongoengine DAO
    """
    class Meta:
        """
        The class defines meta bids model
        """
        model = BidsModel
        interfaces = (Node,)

class AuctionModel(AuctionHouseModel, Document):
    """
    The class defines mongo document model for auction collection
    """
    meta = {"collection": "auction"}
    sold = BooleanField()

class Auction(MongoengineObjectType):
    """
    The class defines configuration with mongoengine DAO
    """
    class Meta:
        """
        The class defines meta auction model
        """
        model = AuctionModel
        interfaces = (Node,)
