"""
The file defines global Auction house model
"""

from mongoengine.fields import (
    StringField,
    BooleanField,
    IntField,
    FloatField,
    ListField,
    EmbeddedDocumentListField,
    EmbeddedDocument)

class EnchantmentModel(EmbeddedDocument):
    """
    The class defines enchantment model
    """
    enchantment = StringField()
    level = IntField()

class BidsModel(EmbeddedDocument):
    """
    The class defines bid model
    """
    auction_id = StringField()
    profile_id = StringField()
    bidder = StringField()
    amount = IntField()
    timestamp = IntField()

class AuctionHouseModel:
    """
    The class defines auction house model
    """
    uuid = StringField()
    coop = ListField(StringField())
    start = FloatField()
    end = FloatField()
    starting_bid = IntField()
    highest_bid_amount = IntField()
    bids = EmbeddedDocumentListField(BidsModel)
    claimed = BooleanField()
    bin = BooleanField()
    lastUpdated = FloatField()

    #Auctionner inforamtions
    auctioneer = StringField()
    profil_id = StringField()

    #Item
    extra = StringField()
    process = BooleanField()
    item_id = StringField()
    enchantments = EmbeddedDocumentListField(EnchantmentModel)
    item_bytes = StringField()
    item_name = StringField()
    category = StringField()

    active = BooleanField()
