"""
The filed defines bin mongo model
"""

from mongoengine.fields import BooleanField, Document

from graphene_mongo import MongoengineObjectType
from graphene import Node

from api.entities.auction_house import AuctionHouseModel

class BinModel(AuctionHouseModel, Document):
    """
    The class defines mongo document model for bin collection
    """
    meta = {"collection": "bin"}
    maybe_sold = BooleanField()

class Bin(MongoengineObjectType):
    """
    The class defines configuration with mongoengine DAO
    """
    class Meta:
        """
        The class defines meta bin model
        """
        model = BinModel
        interfaces = (Node,)
