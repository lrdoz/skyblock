"""
The file defines calls on skyblock API
"""

import logging
import requests

from skyblock.entities.config import Config

LOGGER = logging.getLogger(__name__)

class AuctionService:
    """
    The class defines calls on skyblock API
    """
    @staticmethod
    def get_auction_page(config: Config, page = 0):
        """
        Retrieve auctions from the auction house API

        :params config: Skyblock API configuration
        :params page: Page to be retrieved from the auction house, by default is 0
        :return: Return the auctions retrieved by skyblock API else None
        """
        host = "{0}/{1}".format(config.host_name, "skyblock/auctions")
        params = dict()
        params["page"] = page
        params["key"] = config.key
        LOGGER.debug("Retrieve page %s on skyblock auction house", page)

        try:
            response = requests.get(host, params=params)

            if response.status_code != 200:
                LOGGER.error("Error when try to retrieve auction data. \
                    status_code = %s content = %s", response.status_code, response.content)
                return None
            return response.json()
        except requests.exceptions.RequestException as exception:
            LOGGER.error("Error when try to call skyblock API %s", exception)
            return None
