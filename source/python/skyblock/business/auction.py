"""
File defined business service for skyblock auction
"""

import concurrent.futures
import logging
from typing import List
import time

from skyblock.service.auction import AuctionService
from skyblock.entities.auction import Auction
from skyblock.entities.config import Config

LOGGER = logging.getLogger(__name__)

class AuctionBusinessService(AuctionService):
    """
    The class defines business methods to collect the auction on skyblock API
    """
    def __init__(self, config: Config):
        self.config = config
        self.last_updated = 0

    def get_all_auctions(self) -> List[Auction]:
        """
        Retrieves all auctions found on hypixel skyblock API

        :return: Auctions found
        """
        skyblock_call = self.get_auction_page(self.config, 0)
        if not skyblock_call:
            return None
        pages = list(range(skyblock_call["totalPages"], -1, -1))
        if self.last_updated == skyblock_call["lastUpdated"]:
            LOGGER.error("No update on auctions for this service")
        else:
            self.last_updated = skyblock_call["lastUpdated"]

        LOGGER.info("Going to collect %s pages from the auction house", len(pages))

        auctions = []
        threads = []
        with concurrent.futures.ThreadPoolExecutor() as executor:
            for page in pages:
                threads.append(executor.submit(self.get_auction_page, self.config, page))
        for thread in threads:
            skyblock_call = thread.result()
            if not skyblock_call:
                continue
            #Fromat auction
            for auction in skyblock_call["auctions"]:
                auction["lastUpdated"] = skyblock_call["lastUpdated"]
                auction["bids"] = auction["bids"][-1:] if "bids" in auction and auction["bids"] \
                                else []
                auctions.append(Auction(**auction))

        return auctions

    def timer_update_auction(self):
        """
        Make multi call on hypixel skyblock API, to determine the auction update time

        :return: The auction update time in seconds
        """
        LOGGER.info("Starts determining the auction update time on Skyblock API")
        first_call = self.get_auction_page(self.config, 0)
        while not first_call:
            time.sleep(10)
            first_call = self.get_auction_page(self.config, 0)

        start_time = first_call["lastUpdated"]

        second_call = self.get_auction_page(self.config, 0)
        while True and \
            (not second_call or second_call["lastUpdated"] == start_time):
            time.sleep(10)
            second_call = self.get_auction_page(self.config, 0)

        cycle = second_call["lastUpdated"] - start_time
        cycle /= 1000
        LOGGER.info("The auction update time is to %s second", cycle)
        return cycle
