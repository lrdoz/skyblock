"""
The file defines configuration required to use this package
"""

from pydantic import BaseModel

class Config(BaseModel):
    """
    The class defines the information for skyblock API
    """

    host_name: str
    key: str
