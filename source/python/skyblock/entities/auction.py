"""
The file defines the auction house model
"""

from typing import List, Any, Optional
from pydantic import BaseModel

class Auction(BaseModel):
    """
    Hypixel Auction model
    """
    #Auction information
    uuid: str
    coop: List[str] = []
    start: int
    end: int
    starting_bid: int
    highest_bid_amount: int
    bids: List[Any]
    claimed: bool
    bin: bool = False
    lastUpdated: int = 0

    #Auctionner inforamtions
    auctioneer : Optional [str]
    profil_id: Optional [str]

    #Item
    extra: str
    item_bytes: str
    item_name: str
    category: str

    active: bool = True
