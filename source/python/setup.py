"""
Define python setup for skyblock project
"""
from setuptools import setup, find_packages

setup(name='Skyblock Collector',
      version='1.0',
      description='Skyblock Auction collector',
      author='HALABI Sami',
      author_email='contact@lrdoz.com',
      url="https://gitlab.com/lrdoz/skyblock",
      packages=find_packages())
