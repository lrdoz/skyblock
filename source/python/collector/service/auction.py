"""
The file defines database transaction for auction
"""

from typing import List

from collector.entities.auction import AuctionEntity
from collector.service.auction_house import AuctionHouseService

class AuctionService(AuctionHouseService):
    """
    Auction service for database
    """
    COLLECTION = "auction"

    def get_activate_auctions(self) -> List[AuctionEntity]:
        """
        Retrieve active auctions from the auction house in the database
        """
        auctions = self.database.find({"active": True}).sort("lastUpdated")

        return list(map(lambda x: AuctionEntity(**x), auctions))

    def remove_activate_auctions(self):
        """
        Remove all active auctions from the auction house in the database
        """
        self.database.remove({"active": True})

    def get_not_process_auctions(self) -> List[AuctionEntity]:
        """
        Get the auctions that hasn't not processed in the database
        """
        auctions = self.database.find({"process": {"$exists": False}})

        return list(map(lambda x: AuctionEntity(**x), auctions))
