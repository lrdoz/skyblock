"""
The file defines database transaction for bin
"""

from typing import List

from collector.service.auction_house import AuctionHouseService
from collector.entities.bin import BinEntity

class BinService(AuctionHouseService):
    """
    Bin service for database
    """
    COLLECTION = "bin"

    def get_activate_bins(self) -> List[BinEntity]:
        """
        Retrieve active bins from the auction house in the database
        """
        bins = self.database.find({"active": True}).sort("uuid")

        return list(map(lambda x: BinEntity(**x), bins))

    def remove_activate_bins(self):
        """
        Remove all active bins from the auction house in the database
        """
        self.database.remove({"active": True})

    def get_not_process_bins(self) -> List[BinEntity]:
        """
        Get the bins that hasn't not processed
        """
        bins = self.database.find({"process": {"$exists": False}})

        return list(map(lambda x: BinEntity(**x), bins))
