"""
The file defines database interaction
"""
from typing import Dict, Any, List

import pymongo

class Database:
    """
    The class defines database interaction
    """
    def __init__(self, dao: pymongo.database.Database, collection: str):
        self.database = dao[collection]

    def save(self, data: Dict[str, Any]):
        """

        :param data:
        :return:
        """
        return self.database.update({"_id": data["_id"]}, {"$set": data}, upsert=True)

    def saves(self, datas: List[Dict[str, Any]]):
        """

        :param datas:
        :return:
        """
        ope = lambda data: pymongo.UpdateMany({"_id": data["_id"]}, {"$set": data}, upsert=True)
        operations = list(map(ope, datas))
        return self.database.bulk_write(operations, ordered=False)

    def create_indexs(self, indexs: List[str]):
        """
        Create

        :param indexs: add
        """
        #Create index
        indexs = list(map(pymongo.IndexModel, indexs))
        self.database.create_indexes(indexs)
