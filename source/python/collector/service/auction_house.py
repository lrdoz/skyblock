"""
The file defines database transaction for auction hpuse
"""

from typing import Dict, Any, List

import pymongo

from collector.service.database import Database

class AuctionHouseService(Database):
    """
    Auction house service for database
    """
    COLLECTION = "default_collection"

    def __init__(self, dao: pymongo.database.Database):
        super().__init__(dao, self.COLLECTION)
        indexs = ["start", "end", "active", "item_name", "uuid", "auctioneer", \
            "lastUpdated", "highest_bid_amount"]
        self.create_indexs(indexs)

    def save(self, data: Dict[str, Any]):
        """
        Save the data in the database
        """
        data["_id"] = data["uuid"]
        return super().save(data)

    def saves(self, datas: List[Dict[str, Any]]):
        """
        Save the data in the database
        """
        for data in datas:
            data["_id"] = data["uuid"]
        return super().saves(datas)
