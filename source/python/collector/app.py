"""
The file defines main application
"""

import logging
import sys
import signal
import os
from pymongo import MongoClient

from collector.main import Main

from skyblock.entities.config import Config
from skyblock.business.auction import AuctionBusinessService

LOGGER = logging.getLogger(__name__)

def __log_configuration():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter\
        ('%(asctime)s | %(thread)s | %(name)s | %(levelname)s | %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

def __skyblock_configuration() -> Config:
    host_name = os.getenv("SKYBLOCK_HOSTNAME")
    key_path = os.getenv("KEYS_PATH")

    if not host_name or not key_path or not os.path.exists(key_path):
        not_set = [name for value, name in [(host_name,"HOST_NAME"), (key_path, "KEYS_PATH")] \
            if not value]
        sys.exit("No found env variable for Hypixel API, please set {0} to env var".format(not_set))

    key = open(key_path, "r").readline()
    return Config(**{"host_name": host_name, "key": key})

def __init_database():
    host_database = os.getenv("HOST_DATABASE")
    if not host_database:
        sys.exit("Database hostname not define, set {0} to env var".format("HOST_DATABASE"))
    return MongoClient(host_database)["skyblock"]

if __name__ == "__main__":
    __log_configuration()
    config = __skyblock_configuration()

    auction_service = AuctionBusinessService(config)
    dao = __init_database()
    main = Main(auction_service, dao)

    signal.signal(signal.SIGTERM, main.stop_application)
    signal.signal(signal.SIGINT, main.stop_application)

    LOGGER.info("Starts collect application")
    main.run()
    LOGGER.info("Stop collect application")
