"""
Business process for auction
"""

import logging
from typing import List, NoReturn

from collector.service.auction import AuctionService
from skyblock.entities.auction import Auction


LOGGER = logging.getLogger(__name__)

class AuctionBusinessService(AuctionService):
    """
    Class defines business process for auction
    """
    def update_auctions(self, auctions: List[Auction]) -> NoReturn:
        """
        Update auction on database

        :param auction: Auctions saved
        """
        LOGGER.info("Update %s auction", len(auctions))
        datas = list(map(lambda data: data.dict(exclude_none = True), auctions))
        self.saves(datas)

    def flag_auction_remove_or_end(self) -> NoReturn:
        """
        Find auctions ended or removed on the database
        """
        auctions = self.get_activate_auctions()
        if not auctions:
            LOGGER.warning("No auctions actives")
            return
        last_update = auctions[-1].lastUpdated
        auctions_update = []
        for auction in auctions:
            #No disable auction if is has been updated
            if auction.lastUpdated >= last_update:
                continue

            auction.active = False
            auction.sold = bool(auction.bids)
            auctions_update.append(auction.dict(exclude_none=True))

        if auctions_update:
            self.saves(auctions_update)
