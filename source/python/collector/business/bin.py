"""
Business process for bin
"""

import logging
from typing import List, NoReturn

from skyblock.entities.auction import Auction
from collector.service.bin import BinService

LOGGER = logging.getLogger(__name__)

class BinBusinessService(BinService):
    """
    Class defines business process for bin
    """

    def update_bins(self, bins: List[Auction]) -> NoReturn:
        """
        Update auction on database

        :param bins: Bins saved
        """

        LOGGER.info("Update %s bin", len(bins))
        datas = list(map(lambda data: data.dict(exclude_none = True), bins))
        self.saves(datas)

    def flag_bin_remove_or_end(self) -> NoReturn:
        """
        Find bin ended of removed on the database
        """
        bins = self.get_activate_bins()
        if not bins:
            LOGGER.warning("No bins actives")
            return
        last_update = bins[-1].lastUpdated
        bins_update = []
        for bin_ in bins:
            #No disable auction if is has been updated
            if bin_.lastUpdated == last_update:
                continue

            bin_.active = False
            bin_.maybe_sold = last_update < bin_.end
            bins_update.append(bin_.dict(exclude_none=True))

        if bins_update:
            self.saves(bins_update)
