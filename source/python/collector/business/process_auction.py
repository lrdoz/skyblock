"""
The file defines data extract on auction
"""

import base64
import io
import logging
import traceback
from typing import Dict, Any, NoReturn

import nbt

from collector.business.auction import AuctionBusinessService
from collector.business.bin import BinBusinessService
from collector.entities.auction_house import Enchantment, AuctionHouse

LOGGER = logging.getLogger(__name__)

class ProcessAuctionBusinessService:
    """
    The class defines processes to extract on Auctions and Bins
    """
    def __init__(self, auction_service: AuctionBusinessService, bin_service: BinBusinessService):
        """

        :param auction_service:
        :param bin_service:
        """
        self.auction_service = auction_service
        self.bin_service = bin_service

    def extract_auctions_data(self):
        """
        Extract data on bin
        """
        auctions = self.auction_service.get_not_process_auctions()
        update_auctions = []
        if not auctions:
            return
        LOGGER.info("Going to process %s auction", len(auctions))
        for auction in auctions:
            update_auction = self.extract_nbt(auction)
            if not update_auction:
                continue
            update_auction.process = True
            update_auctions.append(update_auction)
        if update_auctions:
            self.auction_service.update_auctions(update_auctions)

    def extract_bins_data(self) -> NoReturn:
        """
        Extract data on bin
        """
        bins = self.bin_service.get_not_process_bins()
        update_bins = []
        if not bins:
            return
        LOGGER.info("Going to process %s bins", len(bins))
        for bin_ in bins:
            update_bin = self.extract_nbt(bin_)
            if not update_bin:
                continue
            update_bin.process = True
            update_bins.append(update_bin)
        if update_bins:
            self.bin_service.update_bins(update_bins)

    @classmethod
    def extract_nbt(cls, auction: AuctionHouse) -> AuctionHouse:
        """
        Extract item_bytes data on Auction

        :param auction: Auction processed
        :return: Auction updated
        """
        try:
            nbt_data = nbt.nbt.NBTFile(fileobj = io.BytesIO(base64.b64decode(auction.item_bytes)))
            if "i" not in nbt_data and not nbt_data["i"]:
                return auction

            value = cls.nbt_to_json(nbt_data["i"][0])
            extract_attribute = value["tag"]["ExtraAttributes"] \
                if "tag" in value and "ExtraAttributes" in value["tag"] else None
            if not extract_attribute:
                return auction
            auction.item_id = extract_attribute["id"] if "id" in extract_attribute else None
            if "enchantments" in extract_attribute:
                auction.enchantments = [ Enchantment(**{"enchantment": key, "level": value}) \
                                        for key, value in extract_attribute["enchantments"].items()]

        except Exception as exception:
            LOGGER.error("Error when try to extract data on auction id=%s", auction.uuid)
            traceback.print_tb(exception.__traceback__)
            return None
        return auction

    @classmethod
    def nbt_to_json(cls, datas) -> Dict[str, Any]:
        """
        Serialize nbt data to python dict

        :param data: Data processed
        :return: Serialize object
        """
        datas_type = type(datas)
        if datas_type == nbt.nbt.TAG_List:
            result = []
            for data in datas:
                return result.append(cls.nbt_to_json(data))
        elif datas_type == nbt.nbt.TAG_Compound:
            result = {}
            for key, value in datas.items():
                result[key] = cls.nbt_to_json(value)
            return result
        return datas.value
