"""
The file defines main execution of application
"""

import datetime
import logging
import threading
import time
from typing import List, NoReturn

import pymongo

import skyblock.business.auction as sky_auction
from collector.business.auction import AuctionBusinessService
from collector.business.bin import BinBusinessService
from collector.business.process_auction import ProcessAuctionBusinessService

LOGGER = logging.getLogger(__name__)

class Main:
    """
    The class defines main process on collector app
    """
    def __init__(self, skyblock_service: sky_auction.AuctionBusinessService, \
                dao: pymongo.database.Database):

        self.skyblock_service = skyblock_service
        self.auction_service = AuctionBusinessService(dao)
        self.bin_service = BinBusinessService(dao)
        self.run_status: List[bool] = []
        self.can_stop_application: List[bool] = []
        self.process: List[threading.Thread] = []
        self.processing = ProcessAuctionBusinessService(self.auction_service, self.bin_service)
        self.auction_clock = self.skyblock_service.timer_update_auction()

    def run(self) -> NoReturn:
        """
        Starts the execution of the collect application
        """
        confs_process = []

        #Remove old bin and auction active for not have wrong data
        self.bin_service.remove_activate_bins()
        self.auction_service.remove_activate_auctions()

        confs_process.append((self.collect_auction_house_data, "Collect data"))
        confs_process.append((self.extract_data, "Process data"))
        confs_process.append((self.analyse_auction, "Analyse auction"))

        for index, conf_process in enumerate(confs_process):
            self.process.append(threading.Thread(\
                target=self.run_process, args=(index, conf_process[0],), name=conf_process[1]))

        self.run_status = [True for i in range(len(self.process))]
        self.can_stop_application = [True for i in range(len(self.process))]

        list(map(lambda thread: thread.start(), self.process))
        list(map(lambda thread: thread.join(), self.process))

    ##############################
    #   Tag House auction to end #
    ##############################

    def analyse_auction(self):
        """
        Updates auction and bin status if they are finished or deleted
        """
        self.auction_service.flag_auction_remove_or_end()
        self.bin_service.flag_bin_remove_or_end()

    #####################
    #   Processing Data #
    #####################

    def extract_data(self) -> NoReturn:
        """
        Extract auction data
        """
        self.processing.extract_auctions_data()
        self.processing.extract_bins_data()

    #####################
    #   Collecting Data #
    #####################

    def collect_auction_house_data(self) -> NoReturn:
        """
        Collectes auction and bin on the auction house
        """
        LOGGER.info("Starts to collect data on auction house")
        auctions_skyblock = self.skyblock_service.get_all_auctions()

        auctions = []
        bins = []
        for auction in auctions_skyblock:
            ref_list = bins if auction.bin else auctions
            ref_list.append(auction)

        #Save bin
        if bins:
            self.bin_service.update_bins(bins)

        #Save
        if auctions:
            self.auction_service.update_auctions(auctions)
        LOGGER.info("End of data collect on the auction house")

    #########################
    #   Process Exceution   #
    #########################

    def stop_application(self, signum, frame):
        """
        Method to stop collector application
        """
        LOGGER.info("Start to stop application sig=%s frame=%s", signum, frame)
        self.run_status = [False for _ in self.run_status]

        apps_run = [process.name for process in self.process]
        LOGGER.info("Wait process : %s", apps_run)

    def sleep_process(self, start: int, end: int, p_nb: int) -> bool:
        """
        Defined process to keep action on collector app

        :param start: Timestamp when the process starts to apply task
        :param end: Timestamp when the process stop to apply task
        :param p_nb: Process number who want to put on standby
        :return: Return True if process has put on standby else False
        """
        wait = self.auction_clock - end + start
        wait = wait if wait > 0 else 0

        if not self.run_status[p_nb]:
            LOGGER.error("No found process %s ", p_nb)
            return False
        if wait == 0:
            LOGGER.warning("Execution of %s process is too long", p_nb)
            return False

        time.sleep(wait)
        return True

    def run_process(self, p_nb: int, apply) -> NoReturn:
        """
        Defined run execution on collector app

        :param p_nb: Process number on collector app
        :param apply: Function call for apply execution
        """
        while self.run_status[p_nb]:
            self.can_stop_application[p_nb] = False
            start = datetime.datetime.now().timestamp()
            apply()
            end = datetime.datetime.now().timestamp()

            self.can_stop_application[p_nb] = True
            self.sleep_process(start, end, p_nb)
