"""
The file defines business auction model
"""

from collector.entities.auction_house import AuctionHouse

class AuctionEntity(AuctionHouse):
    """
    Class defines business auction entity
    """
    sold: bool = False
