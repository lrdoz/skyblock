"""
Defined global business auction entity
"""

from typing import Optional, List
from pydantic import BaseModel

from skyblock.entities.auction import Auction

class Enchantment(BaseModel):
    """
    Class defines enchantment can be apply on auction item
    """
    enchantment: str
    level: int

class AuctionHouse(Auction):
    """
    Class defines auction item
    """
    _id: str
    process: bool = False

    #Item
    item_id: Optional[str]
    enchantments: Optional[List[Enchantment]]
