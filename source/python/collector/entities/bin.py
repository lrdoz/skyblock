"""
The file defines business bin model
"""

from collector.entities.auction_house import AuctionHouse


class BinEntity(AuctionHouse):
    """
    Class defines business bin entity
    """
    maybe_sold: bool = False
